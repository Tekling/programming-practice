package dao;

import java.util.List;


import domain.Customer;

public interface CustomerDao {

	public List <Customer> getCustomer();
	public Customer createCustomer(String username, String password, String fname, String lname, String email);
	
	public Customer updateCustomer(String name);
	int deleteCustomer(Customer delete);
	
	public Customer getCustomerById(int id);
	public String getCustomerIdString(int id);
	
	public Customer getCustomerByUser(String user);
	String getCustomerUserString(String user);
	
	public Customer getCustomerAcc();
	String getCustomerAccString();
	
	public Customer getCustomerAccById(int id);
	String getCustomerAccStringById(int id);
	
	public void sendBlob(String file, int id);
	
	public Customer getCustomerAccByUser(String user);
	String getCustomerAccStringByUser(String user);
	
	public void updateCustomerReinA(int amount, String name);
	public void updateCustomerReinS(String status, String name);
	
}
