package com.jml.dao;

import java.util.List;

import com.jml.domain.*;

public interface CaveDao {
	
	public List<Cave> getCaves();
	public Cave getCaveById(int id);

}
