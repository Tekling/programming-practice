package com.jml.dao;

import java.util.List;

import com.jml.domain.Bear;

public interface BearDao {

	public List<Bear> getBears();
	
	public Bear getBearById(int id);
	
	public int feedBear(int bearId, int hiveId, int amt);
	
	public int createBear(Bear bear);
	
}
