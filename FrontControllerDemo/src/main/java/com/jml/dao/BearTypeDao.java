package com.jml.dao;

import java.util.List;

import com.jml.domain.BearType;

public interface BearTypeDao {
	
	public List<BearType> getBearTypes();
	public BearType getBearTypeById(int id);

}
