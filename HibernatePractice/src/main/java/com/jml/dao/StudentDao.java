package com.jml.dao;

import java.util.List;

import com.jml.domain.Flashcard;
import com.jml.domain.Student;

public interface StudentDao {
	public List <Student> getStudent();
	public Student getStudentById(int id);
	public int addStudent(Student s);
	public void updateStudent(Student s);
	public void deleteStudent(Student s);

}
