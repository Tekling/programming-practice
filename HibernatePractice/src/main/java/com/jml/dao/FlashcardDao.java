package com.jml.dao;

import java.util.List;

import com.jml.domain.Flashcard;

public interface FlashcardDao {
	public List <Flashcard> getFlashcards();
	public Flashcard getFlashcardById(int id);
	public int addFlashcard(Flashcard f);
	public void updateFlashcard(Flashcard f);
	public void deleteFlashcard(Flashcard f);
}
